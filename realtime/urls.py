from django.conf.urls import include, url
from django.contrib import admin

from realtime.views import RealtimeView

urlpatterns = [
    url(r'^$', PhotoView.as_view(), name="home"),
    url(r'^realtime/$', RealtimeView.as_view(), name="realtime"),
    url(r'^feedback/$', FeedbackView.as_view(), name="feedback"),
    url(r'^admin/', include(admin.site.urls)),
]
