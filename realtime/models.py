from django.db import models


class Person(models.Model):
    name = models.CharField(max_length=255)
    lat = models.DecimalField(max_digits=9, decimal_places=6)
    lng = models.DecimalField(max_digits=9, decimal_places=6)
    updated_on = models.DateTimeField("Updated on", auto_now=True)
    description = models.TextField("Description")

    class Meta:
        verbose_name = "Person"
        verbose_name_plural = "Persons"
        ordering = ['name']

    def __str__(self):
        return self.name
