import json
import requests

from realtime.models import Person
from realtime import settings as person_settings


def get_location(pid):
    """
    GETS the latest person location
    """

    # TODO: IMPLEMENTED METHOD
    return 

def save_person_latest_location(pid):
    """
    Updates DB with a persons latest location
    """
    new_location = get_location(pid)
    Person.objects.filter(pk=pid).update(location=new_location)
