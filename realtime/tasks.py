from celery.task.schedules import crontab
from celery.decorators import periodic_task
from celery.utils.log import get_task_logger

from photos.utils import save_person_latest_location

logger = get_task_logger(__name__)


@periodic_task(
    run_every=(crontab(minute='*/1')),
    name="fetch_person_latest_location",
    ignore_result=True
)
def fetch_person_latest_location():
    """
    GETS and updates DB with a persons latest location
    """
    save_person_latest_location()
    logger.info("Location Saved")
