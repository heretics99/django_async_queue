from django.views.generic.list import ListView

from realtime.models import Person
# from feedback.forms import FeedbackForm


# The final view on the website:
# 1. A map showing the locations of persons in realtime.
# 2. Each person will have a trail line showing last 5 locations for this person. Something like Snake's game on Nokia phones.
# 3. Each person gets a new random location within 200mts of their present location, with a MAX variation of 10 degrees from their previous location.
# 4. Using websockets, the new position gets updated on the frontend.


class RealtimeView(ListView):
    model = Person
    template_name = 'realtime/realtime_map.html'
    paginate_by = 24

    def get_context_data(self, **kwargs):
        context = super(RealtimeView, self).get_context_data(**kwargs)
        return context
