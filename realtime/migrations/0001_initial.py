# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('lat', models.DecimalField(max_digits=9, decimal_places=6)),
                ('lng', models.DecimalField(max_digits=9, decimal_places=6)),
                ('updated_on', models.DateTimeField(auto_now=True, verbose_name=b'Updated on')),
                ('description', models.TextField(verbose_name=b'Description')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Person',
                'verbose_name_plural': 'Persons',
            },
        ),
    ]
